import sys
import math
from functools import partial
from math import sqrt
from time import sleep

# todo: remove me:
# todo: remove me:
# todo: remove me:
with open(sys.argv[2], "w"):
    pass

sys.stderr = open(sys.argv[3], "w")

input_line_nb = 0

print("{}".format(sys.argv), file=sys.stderr)

input_line_nb = [0]


def get_input_from_file():
    # debug("reading {}".format(sys.argv[1]))
    filename = sys.argv[1]
    with open(filename, "r") as input_file:
        for _ in range(input_line_nb[0]):
            input_file.readline()
        line = input_file.readline()

    for i in range(50):
        if line == "":
            debug("sleeping")
            sleep(0.2)
            with open(filename, "r") as input_file:
                for _ in range(input_line_nb[0]):
                    input_file.readline()
                line = input_file.readline()
        else:
            break
    if line == "":
        debug("empty_line reading {}".format(filename))
    debug(line=line)
    input_line_nb[0] += 1
    return line

classic_input = input
input = get_input_from_file


def display(*args, **kwargs):
    debug(*args, **kwargs)
    with open(sys.argv[2], "a") as output_file:
        output_file.write(*args)
        output_file.write("\n")

# todo: remove me:
# todo: remove me:
# todo: remove me:

# MAP
X_SIZE = 17630
Y_SIZE = 9000

BASE_RANGE = 5000

# TURNS
RED_MONSTERS_TURN = 280

# MOVES
ATTACK_RANGE = 800
MOVE_RANGE = 800

# SPELLS
WIND_RANGE = 1280
WIND_PUSHBACK_DISTANCE = 2200
SHIELD_MAX_DURATION = 12
SHIELD_RANGE = 2200
CONTROL_RANGE = 2200
SPELL_MANA_COST = 10

ROLE_BASE_POSITIONS = {
    "FARM_1": (7800, 500),
    "FARM_2": (12000, 600),
    "FARM_3": (4500, 7500),
    "DEFENSE_START_1": (6000, 1500),
    "DEFENSE_START_2": (8000, 5000),
    "DEFENSE_START_3": (4500, 4500),
    "DEFENSE_MID_1": (2300, 750),
    "DEFENSE_MID_2": (1200, 2700),
    "ATTACK": (15000, 6400),
}
HERO_ROLES = [
    "FARM_1",
    "FARM_2",
    "FARM_3",
    "DEFENSE_START_1",
    "DEFENSE_START_2",
    "DEFENSE_START_3",
    "DEFENSE_MID_1",
    "DEFENSE_MID_2",
    "ATTACK"
]
HERO_NAMES = ["PABLO", "BRETO", "CHEPO"]

controled_enemies = []
enemy_in_attack = False
locks = {}


class O:
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __repr__(self):
        return "{}({})".format(self.__class__.__name__,
                               ", ".join(
                                   "{}={}".format(key, getattr(self, key))
                                   for key in dir(self)
                                   if not key.startswith("_") and not hasattr(getattr(self, key), "__call__")
                               )
                               )


class Hero(O):
    def cast_wind(self, x, y):
        if not hasattr(self, "action") or "SPELL" not in self.action:
            base.mana -= SPELL_MANA_COST
        self.action = "SPELL WIND {} {} {} wind".format(x, y, self.name)

    def cast_shield(self, target_id):
        if not hasattr(self, "action") or "SPELL" not in self.action:
            base.mana -= SPELL_MANA_COST
        self.action = "SPELL SHIELD {} {} shield".format(target_id, self.name)

    def cast_control(self, target):
        if not hasattr(self, "action") or "SPELL" not in self.action:
            base.mana -= SPELL_MANA_COST
        down_or_right = target.y < Y_SIZE / 2
        points = [(4250, 1500), (1500, 4250)]
        x, y = points[down_or_right]
        if enemy_base.is_red():
            x = X_SIZE - x
            y = Y_SIZE - y
        controled_enemies.append(target.id)
        self.action = "SPELL CONTROL {} {} {} {} control".format(target.id, x, y, self.name)

    def move(self, x, y):
        if hasattr(self, "action") and "SPELL" in self.action:
            base.mana += SPELL_MANA_COST
        self.action = "MOVE {} {} {} move".format(x, y, self.name)

    def wait(self):
        if hasattr(self, "action") and "SPELL" not in self.action:
            base.mana += SPELL_MANA_COST
        self.action = "WAIT {} nothing to do...".format(self.name)

    def take_decision(self):
        role_x, role_y = self.get_role_position()

        controlable_monster = self.get_controlable_monster()
        monster_to_attack = self.get_monster_to_attack()
        closest_monster_attacking = self.get_closest_monster_attacking()
        farmeable_monster = self.get_farmeable_monster()
        monster_to_wind = self.get_monster_to_wind()
        monster_to_shield = self.get_monster_to_shield()
        locked_monster = self.get_locked_monster()
        enemies_to_wind = self.get_enemies_to_wind()
        should_go_attack = self.get_should_go_attack()

        should_self_shield = self.get_self_shield()

        if should_self_shield:
            debug(self.name, "SELF SHIELD", self)
            self.cast_shield(self.id)
        elif enemies_to_wind:
            debug(self.name, "WINDING E", enemies_to_wind)
            self.cast_wind(base.x, base.y)
        elif monster_to_shield:
            debug(self.name, "SHIELDING", monster_to_shield)
            self.cast_shield(monster_to_shield.id)
        elif monster_to_wind:
            debug(self.name, "WINDING M", monster_to_wind)
            self.cast_wind(enemy_base.x, enemy_base.y)
        elif controlable_monster:
            debug(self.name, "CONTROLING", controlable_monster)
            self.cast_control(controlable_monster)
        elif should_go_attack:
            debug(self.name, "LEEEEROY", self)
            self.move(role_x, role_y)
        elif monster_to_attack:
            debug(self.name, "ATTACK MONSTER", monster_to_attack)
            self.attack(monster_to_attack)
        elif locked_monster:
            debug(self.name, "Continue support vs", locked_monster)
            self.attack(locked_monster)
        elif self.x != role_x and self.y != role_y:
            debug(self.name, "back to position")
            self.move(role_x, role_y)
        elif farmeable_monster:
            debug("farming", farmeable_monster)
            locks[self.name] = farmeable_monster.id
            self.attack(farmeable_monster)
        elif closest_monster_attacking:
            debug(self.name, "Support defense vs", closest_monster_attacking)
            locks[self.name] = closest_monster_attacking.id
            self.attack(closest_monster_attacking)
        else:
            debug(self.name, "guard")
            self.move(role_x, role_y)

    def get_role_position(self):
        return ROLE_BASE_POSITIONS[self.role]

    def get_closest_monster_attacking(self):
        monsters_attacking = [
            monster for monster in monsters
            if monster.threat_for == 1
        ]
        if not monsters_attacking:
            return None
        return min(monsters_attacking, key=lambda monster: monster.distance_to(self))

    def get_monster_to_shield(self):
        shieldable_monsters = [monster for monster in monsters
                               if self.can_shield(monster)
                               and monster.will_reach_base()
                               and monster.nb_turns_to_reach_enemy_base() < 12]
        if shieldable_monsters:
            result = min(shieldable_monsters, key=lambda x: x.distance_to(enemy_base))
        else:
            result = None
        return result

    def get_self_shield(self):
        return (
            self.shield_life == 0
            and self.distance_to(enemy_base) < 5000
            and any(enemy for enemy in enemies if self.distance_to(enemy) < WIND_RANGE + MOVE_RANGE * 2))

    def get_enemies_to_wind(self):
        shielded_monster_threatening = [monster for monster in monsters
                                        if (monster.distance_to(enemy_base) < 5000
                                            and monster.will_reach_base_with_shield())]

        should_wind = [enemy for enemy in enemies if (
            any(monster.distance_to(enemy) < MOVE_RANGE + ATTACK_RANGE for monster in shielded_monster_threatening)
            and self.can_wind(enemy))]

        if should_wind:
            result = should_wind[0]
        else:
            result = None
        return result

    def get_monster_to_wind(self):
        should_wind = [monster for monster in monsters if (monster.distance_to(base) < 4000
                                                           and self.can_wind(monster)
                                                           and monster.health > 3)]
        if should_wind:
            result = should_wind[0]
        else:
            result = None
        return result

    def get_monster_to_attack(self):
        monsters_to_attack = []
        for monster in monsters:
            weak_monster = monster.health <= 10
            closest_hero = min(heroes, key=lambda hero: hero.distance_to(monster))
            hero_is_the_closest = (self.id == closest_hero.id)
            monster_attacking_base = (monster.threat_for == 1)
            if (hero_is_the_closest
                and (weak_monster or monster_attacking_base)
                and (monster.distance_to(enemy_base) > 5000)):
                monsters_to_attack.append(monster)

        if not monsters_to_attack:
            return None

        return min(monsters_to_attack, key=lambda monster: monster.distance_to(base))

    def get_should_go_attack(self):
        role_x, role_y = self.get_role_position()
        return (self.role == "ATTACK" and self.distance_to(O(x=role_x, y=role_y)) > 4000)

    def get_controlable_monster(self):
        strong_monsters = [
            monster for monster in monsters
            if self.can_control(monster)
               and (monster.def_control_cdtion() or monster.off_control_cdtion())
        ]
        if not strong_monsters:
            return None
        return min(strong_monsters, key=lambda monster: monster.distance_to(base))

    def get_locked_monster(self):
        if self.name not in locks:
            return None
        else:
            locked_monsters = [monster for monster in monsters if monster.id == locks[self.name]]
            debug(locked_monsters=locked_monsters)
            if locked_monsters:
                return locked_monsters[0]
            else:
                locks[self.name] = None
                return None

    def get_farmeable_monster(self):
        if enemy_in_attack or self.role == "ATTACK":
            return None
        farmeable_monsters = [
            monster for monster in monsters if monster.distance_to(self) < 4000
        ]
        if not farmeable_monsters:
            return None
        return min(farmeable_monsters, key=lambda m: m.distance_to(self))

    def nb_windable(self, monsters):
        return len(
            [monster for monster in monsters if distance_to(self, monster) < WIND_RANGE and monster.shield_life == 0])

    def can_shield(self, target):
        return (self.distance_to(target) <= SHIELD_RANGE
                and base.mana >= SPELL_MANA_COST
                and target.shield_life == 0)

    def can_control(self, target):
        return (self.distance_to(target) <= CONTROL_RANGE
                and base.mana >= SPELL_MANA_COST
                and target.shield_life == 0
                and target.is_controlled != 1
                and target.id not in controled_enemies)

    def can_wind(self, target):
        return (self.distance_to(target) <= WIND_RANGE
                and base.mana >= SPELL_MANA_COST
                and target.shield_life == 0)

    def attack(self, monster):
        self.target = monster.id
        self.move(monster.next_x, monster.next_y)


class Monster(O):
    def set_next_pos(self):
        self.next_x = self.x + self.vx
        self.next_y = self.y + self.vy

    def nb_turns_to_reach_enemy_base(self):
        return round((self.distance_to(enemy_base) - 300) / 400) + 1

    def get_nb_nearby_enemies(self):
        return len([enemy for enemy in enemies if self.distance_to(enemy) < 1500])

    def turns_to_live(self):
        return round(self.health / max(self.get_nb_nearby_enemies(), 1))

    def will_reach_base(self):
        return (self.turns_to_live() > self.nb_turns_to_reach_enemy_base()
                and self.threat_for == 2)

    def will_reach_base_with_shield(self):
        return self.health > self.get_nb_nearby_enemies() * 2 + 7 and self.shield_life > 0

    def def_control_cdtion(self):
        return (
            self.threat_for == 1
            and self.health >= 15
            and enemy_in_attack
            and self.distance_to(base) < 5000
        )

    def off_control_cdtion(self):
        return (
            self.threat_for != 2
            and self.health >= 20
            and self.distance_to(enemy_base) < 9000
        )


class Enemy(O):
    pass


class Base(O):
    def is_blue(self):
        return self.x == 0

    def is_red(self):
        return self.x != 0


def debug(*args, **kwargs):
    parts = [
                "{}".format(arg) for arg in args
            ] + [
                "{}={}".format(key, value) for key, value in kwargs.items()
            ]
    print("DEBUG: " + " -- ".join(parts), file=sys.stderr)


def output(heroes):
    for hero in heroes:
        display(hero.action)


def init_entities():
    health, mana = [int(j) for j in input().split()]
    enemy_health, enemy_mana = [int(j) for j in input().split()]
    entity_count = int(input())
    entities = []
    entities_class = [Monster, Hero, Enemy]
    debug(entity_count)
    for i in range(entity_count):
        id, type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for = [int(j) for j in
                                                                                             input().split()]
        entities.append(entities_class[type](
            id=id,
            type=type,
            x=x,
            y=y,
            shield_life=shield_life,
            is_controlled=is_controlled,
            health=health,
            vx=vx,
            vy=vy,
            near_base=near_base,
            threat_for=threat_for,
        ))
    monsters = [entity for entity in entities if entity.type == 0]
    heroes = [entity for entity in entities if entity.type == 1]
    enemies = [entity for entity in entities if entity.type == 2]
    base.health = health
    base.mana = mana
    base.turn += 1
    return base, heroes, enemies, monsters


def distance_to(entity1, entity2):
    return sqrt(pow(entity1.x - entity2.x, 2) + pow(entity1.y - entity2.y, 2))


O.distance_to = distance_to
base_x, base_y = [int(i) for i in input().split()]
heroes_number = int(input())

base = Base(x=base_x, y=base_y, turn=-1)
O.dist_to_base = partial(distance_to, base)
if base.is_red():
    ROLE_BASE_POSITIONS = {
        key: (X_SIZE - pos_x, Y_SIZE - pos_y)
        for key, (pos_x, pos_y) in ROLE_BASE_POSITIONS.items()
    }

enemy_base = Base(x=17630 if base_x == 0 else 0, y=9000 if base_y == 0 else 0)
O.dist_to_enemy_base = partial(distance_to, enemy_base)

# game loop
while True:
    base, heroes, enemies, monsters = init_entities()
    controled_enemies = []

    for role_id, hero in enumerate(heroes):
        hero.role = HERO_ROLES[role_id + (3 if base.turn > 45 else 0) + (3 if base.turn > 100 else 0)]
        hero.name = HERO_NAMES[role_id]

    for id, monster in enumerate(monsters):
        monster.set_next_pos()

    if any(enemy.distance_to(base) < 9000 for enemy in enemies):
        enemy_in_attack = True

    for hero in heroes:
        hero.take_decision()

    output(heroes)
