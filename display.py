from __future__ import division, print_function
from time import sleep
import math
from math import sqrt
import pygame
from pygame import Color
import sys
import subprocess
from random import randrange, random
# MAP
from helpers.imports import O, debug


def distance_to(entity1, entity2):
    return sqrt(pow(entity1.x - entity2.x, 2) + pow(entity1.y - entity2.y, 2))


O.distance_to = distance_to

X_SIZE = 17630
Y_SIZE = 9000

BASE_RANGE = 5000

# TURNS
RED_MONSTERS_TURN = 280

# MOVES
ATTACK_RANGE = 800
MOVE_RANGE = 800

# SPELLS
WIND_RANGE = 1280
WIND_PUSHBACK_DISTANCE = 2200
SHIELD_MAX_DURATION = 12
SHIELD_RANGE = 2200
CONTROL_RANGE = 2200
SPELL_MANA_COST = 10


# pygame.display.init()

red = Color(255, 0, 0, 50)
green = Color(0, 255, 0, 50)
blue = Color(0, 0, 255, 50)
dark_blue = Color(0, 0, 128, 50)
dark_red = Color(128, 0, 0, 50)
white = Color(255, 255, 255, 50)
black = Color(0, 0, 0, 50)
pink = Color(255, 200, 200, 50)

SCALE=20
def conv(x, y=None):
    if y is not None:
        return int(x / SCALE), int(y / SCALE)

    return int(x / SCALE)


class Monster(O):
    pass


class Hero(O):
    pass


class Enemy(O):
    pass


class Base(O):
    pass


entities_class = [Monster, Hero, Enemy]
screen = pygame.display.set_mode(conv(X_SIZE, Y_SIZE))

from_situation = [
    "3 0",
    "3 0",
    "3",
    "0 2 1131 1131 0 0 -1 -1 -1 -1 -1",
    "1 2 1414 849 0 0 -1 -1 -1 -1 -1",
    "2 2 849 1414 0 0 -1 -1 -1 -1 -1",
    "3 1 16499 7569 0 0 -1 -1 -1 -1 -1",
    "4 1 16216 8151 0 0 -1 -1 -1 -1 -1",
    "5 1 16781 7586 0 0 -1 -1 -1 -1 -1",
]


def re_draw(monsters, heroes, enemies):
    screen.fill(black)
    pygame.draw.circle(screen, red, (0, 0), conv(5000), 0)
    pygame.draw.circle(screen, blue, conv(X_SIZE, Y_SIZE), conv(5000), 0)
    for monster in monsters:
        c = 255-(10*monster.health)
        pygame.draw.circle(screen, (c, c, c), conv(monster.x, monster.y), conv(200))
    for hero in heroes:
        pygame.draw.circle(screen, dark_blue, conv(hero.x, hero.y), conv(800))
    for enemy in enemies:
        pygame.draw.circle(screen, dark_red, conv(enemy.x, enemy.y), conv(800))
    pygame.display.update()


def format_entity(entity, change_types):
    return "{} {} {} {} {} {} {} {} {} {} {}".format(
        entity.id,
        entity.type if not change_types else 1 if entity.type == 2 else 2 if entity.type == 1 else 0,
        entity.x,
        entity.y,
        entity.shield_life,
        entity.is_controlled,
        entity.health,
        entity.vx,
        entity.vy,
        entity.near_base,
        entity.threat_for
    )


def do_base_inputs(base, code_input_name, enemy_base, enemy_input_name):
    with open(code_input_name, "a") as code_input:
        print("{} {}".format(base.x, base.y), file=code_input)
        print("3", file=code_input)
    with open(enemy_input_name, "a") as enemy_input:
        print("{} {}".format(enemy_base.x, enemy_base.y), file=enemy_input)
        print("3", file=enemy_input)


def handle(hero, base, command, entities, reversed):
    if command.startswith("WAIT"):
        pass
    elif command.startswith("MOVE "):
        requested_x, requested_y = [int(i) for i in command.split(" ")[1:3]]
        dx, dy = requested_x-hero.x, requested_y-hero.y
        d = distance_to(O(x=0, y=0), O(x=dx, y=dy))
        if d > 0:
            hero.x += int((min(d, MOVE_RANGE) / d) * dx)
            hero.y += int((min(d, MOVE_RANGE) / d) * dy)
    elif command.startswith("SPELL "):
        spell = command.split(" ")[1]
        if spell == "WIND":
            x, y = command.split(" ")[2:4]
            x, y = int(x), int(y)
            targets = [
                entity
                for entity in entities
                if entity.type != (1 if not reversed else 2)
                   and entity.distance_to(hero) < 1280
                   and entity.shield_life == 0
            ]
            if not targets:
                debug("no target for wind", hero.id)
            else:
                for target in targets:
                    dx, dy = x - target.x, y - target.y
                    d = distance_to(O(x=0, y=0), O(x=dx, y=dy))
                    if d > 0:
                        target.x += int((min(d, 2200) / d) * dx)
                        target.y += int((min(d, 2200) / d) * dy)
                debug(spell, x, y)
        elif spell == "SHIELD":
            entity_id = command.split(" ")[2]
            target = [entity for entity in entities if entity.id == entity_id]
            if target and target[0].shield_life == 0 and target[0].distance_to(hero) < 2200:
                target[0].shield_life = 2
                debug(spell, entity_id)
            else:
                debug("BAD TARGET FOR {}".format(hero.name, spell, entity_id, target))
        elif spell == "CONTROL":
            entity_id, x, y = command.split(" ")[2:5]
            x, y = int(x), int(y)
            target = [entity for entity in entities if entity.id == entity_id]
            if target and target[0].shield_life == 0 and target[0].distance_to(hero) < 2200:
                dx, dy = x - target[0].x, y - target[0].y
                d = distance_to(O(x=0, y=0), O(x=dx, y=dy))
                if d > 0:
                    target[0].dx = int((min(d, 400) / d) * dx)
                    target[0].dy = int((min(d, 400) / d) * dy)
            else:
                debug("BAD TARGET FOR {}".format(hero.name, spell, entity_id, target))

            debug(spell, entity_id, x, y)
        else:
            debug(spell)
    else:
        debug("Not supported", command)


def do_inputs(base, code_input_name, enemies, enemy_base, enemy_input_name, heroes, entities):
    inputs = generate_input(base, enemy_base, entities, heroes, False)
    for line in inputs:
        with open(code_input_name, "a") as code_input:
            print(line, file=code_input)

    inputs = generate_input(enemy_base, base, entities, enemies, True)
    for line in inputs:
        with open(enemy_input_name, "a") as enemy_input:
            print(line, file=enemy_input)


def get_visible_entities(entities, heroes, base):
    visible_entities = [
        entity
        for entity in entities
        if (
            any(hero.distance_to(entity) < 2200 for hero in heroes) or base.distance_to(entity) < 6000
        ) and not (
            entity.x < 0 or
            entity.y < 0 or
            entity.x > X_SIZE or
            entity.y > Y_SIZE
        )
    ]
    return visible_entities


def generate_input(base, enemy_base, entities, heroes, change_types):
    visible_entities = get_visible_entities(entities, heroes, base)
    inputs = [
        "{} {}".format(base.health, base.mana),
        "{} {}".format(enemy_base.health, enemy_base.mana),
        "{}".format(len(visible_entities)),
        *[format_entity(entity, change_types) for entity in visible_entities]
    ]
    return inputs


def main():
    line_nb = 0
    enemy_line_nb = 0
    code_input_name = "code_input.txt"
    for name in [
        "code_input.txt",
        "code_output.txt",
        "code_debug.txt",
        "enemy_input.txt",
        "enemy_output.txt",
        "enemy_debug.txt",
    ]:
        with open(name, "w"):
            pass
    code_output_name = "code_output.txt"
    enemy_input_name = "enemy_input.txt"
    enemy_output_name = "enemy_output.txt"

    subprocess.Popen([
        "python3",
        "code.py",
        "code_input.txt",
        "code_output.txt",
        "code_debug.txt",
    ])
    subprocess.Popen([
        "python3",
        "code.py",
        "enemy_input.txt",
        "enemy_output.txt",
        "enemy_debug.txt",
    ])

    entities = []
    health, mana = [int(j) for j in from_situation[0].split()]
    base = Base(health=health, mana=mana, x=X_SIZE, y=Y_SIZE)
    enemy_health, enemy_mana = [int(j) for j in from_situation[1].split()]
    enemy_base = Base(health=enemy_health, mana=enemy_mana, x=0, y=0)
    entity_count = int(from_situation[2])

    do_base_inputs(base, code_input_name, enemy_base, enemy_input_name)
    for line in from_situation[3:]:
        id, type, x, y, shield_life, is_controlled, health, vx, vy, near_base, threat_for = [
            int(j) for j in line.split()
        ]
        entities.append(entities_class[type](
            id=id,
            type=type,
            x=x,
            y=y,
            shield_life=shield_life,
            is_controlled=is_controlled,
            health=health,
            vx=vx,
            vy=vy,
            near_base=near_base,
            threat_for=threat_for,
        ))

    for turn in range(0, 220):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        # debug(turn, len(entities))
        monsters = [entity for entity in entities if entity.type == 0]
        heroes = [entity for entity in entities if entity.type == 1]
        enemies = [entity for entity in entities if entity.type == 2]
        re_draw(monsters, heroes, enemies)

        do_inputs(base, code_input_name, enemies, enemy_base, enemy_input_name, heroes, entities)

        add_monsters(turn, entities)

        update_monsters(entities, base, enemy_base)

        for hero in heroes:
            command, line_nb = get_command(code_output_name, line_nb)
            handle(hero, base, command, entities, False)

        for enemy in enemies:
            command, enemy_line_nb = get_command(enemy_output_name, enemy_line_nb)
            handle(enemy, enemy_base, command, entities, True)


        remove_monsters(entities, base, enemy_base)
        sleep(0.05)


def remove_monsters(entities, base, enemy_base):
    base.health -= 8 * len([
        entity for entity in entities if entity.type == 0 and entity.distance_to(base) < 300
    ])
    enemy_base.health -= 8 * len([
        entity for entity in entities if entity.type == 0 and entity.distance_to(enemy_base) < 300
    ])
    entities[:] = [
        e for e in entities if e.type != 0 or not (
            e.health <= 0 or
            e.distance_to(base) < 300 or
            e.distance_to(enemy_base) < 300
        )
    ]


def update_monsters(entities, base, enemy_base):
    for entity in entities:
        if entity.type == 0:
            heroes_nearby = len([
                e for e in entities if e.type == 1 and e.distance_to(entity) < 800
            ])
            base.mana += min(2 * heroes_nearby, entity.health)
            enemies_nearby = len([
                e for e in entities if e.type == 2 and e.distance_to(entity) < 800
            ])
            enemy_base.mana += min(2 * enemies_nearby, entity.health)

            entity.health -= 2 * (heroes_nearby + enemies_nearby)
            entity.x += entity.vx
            entity.y += entity.vy
            if entity.distance_to(base) < 5000:
                dx, dy = base.x - entity.x, base.y-entity.y
                d = distance_to(O(x=0, y=0), O(x=dx, y=dy))
                if d > 0:
                    entity.x += int((min(d, 400) / d) * dx)
                    entity.y += int((min(d, 400) / d) * dy)


def add_monsters(turn, entities):
    monster_x = randrange(int(1/3*X_SIZE), int(2/3*X_SIZE))
    angle = random() * 2 * 3.1415
    vx, vy = int(math.sin(angle) * 400), int(math.cos(angle) * 400)
    entities.append(new_monster(monster_x, 0, vx, vy, turn))
    entities.append(new_monster(X_SIZE-monster_x, Y_SIZE, -vx, -vy, turn))


monster_current_id = [10]


def new_monster(x, y, vx, vy, turn):
    monster_current_id[0] += 1
    return Monster(
        id=monster_current_id[0],
        type=0,
        x=x,
        y=y,
        shield_life=0,
        is_controlled=0,
        health=10 if turn < 45 else randrange(14, 17) if turn < RED_MONSTERS_TURN else randrange(19, 21),
        vx=vx,
        vy=vy,
        near_base=0,
        threat_for=0
    )


def get_command(filename, line_nb):
    with open(filename, "r") as command_file:
        for _ in range(line_nb):
            command_file.readline()
        line = command_file.readline()

    for i in range(100):
        if line == "":
            # debug("sleeping")
            sleep(0.1)
            with open(filename, "r") as command_file:
                for _ in range(line_nb):
                    command_file.readline()
                line = command_file.readline()
        else:
            break
    if line == "":
        debug("empty_line reading {}".format(filename))

    return line, line_nb + 1


main()
